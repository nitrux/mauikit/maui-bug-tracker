# Bug Repository for MauiKit and MauiKit Applications.

![](https://mauikit.org/wp-content/uploads/2020/02/maui_project_logo-min_2.png)

This repository tracks the following software:

- [MauiKit](https://invent.kde.org/kde/mauikit)
- [Pix](https://invent.kde.org/kde/maui-pix)
- [Index](https://invent.kde.org/kde/index-fm)
- [VVave](https://invent.kde.org/kde/vvave)
- [Station/Commander](https://invent.kde.org/kde/maui-station)
- [Nota](https://invent.kde.org/kde/nota)
- [Contacts](https://invent.kde.org/kde/maui-dialer)
- [Library](https://invent.kde.org/kde/maui-library)
- [Buho](https://invent.kde.org/kde/buho)


# How to report bugs
Nitrux uses GitHub to keep track of bugs in MauiKit, its applications, and its fixes.

## Determine if the bug is a bug
You should not file a bug if you are:

1. Requesting new software.
2. _Discussing features, existing policy, proposing features, or ideas._
3. Filing a bug against software not provided by MauiKit.

## Submitting a Bug

Open a new issue and select "Bug report" and then click on "Get started."

## Notes

Some extra options you can use to make your bug report more complete:

1. If the bug is a security vulnerability: Please check this only if your bug report describes behavior that could be exploited to compromise your security or safety, as well as cause issues such as identity theft or "hi-jacking."

2. Include an attachment: add supporting attachments to explain or help others reproduce the bug, this might consist of a screenshot, a video capture of the problem, or a sample document that triggers the fault.

# How to add a feature request
To add features, do the following:

1. Open a new issue and click on "Open a regular issue."
2. Use the title [Feature] [App_Name] short_feature_description. You can be more thorough in the accompanying comment.

©2019 Nitrux Latinoamericana S.C.
